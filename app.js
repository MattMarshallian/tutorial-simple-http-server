'use strict';

const http = require('http')
const port = 3000

const requestHandler = (request, response) => {
    const body = request.method +": "+ request.url;
    console.log(body);
    response.end(body);
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }

    console.log(`server is listening on ${port}`)
})